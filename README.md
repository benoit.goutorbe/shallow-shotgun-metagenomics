Scripts regarding my project on shallow shotgun metagenomics.
# Content

example : 
 * a minimal example to reproduce the pipeline on other data.

Simulations : 
 * retrieve profiles on *curatedMetagenomicData*
 * transalte to UHGG taxonomy
 * simulate sequencing reads with *Grinder*
 * bioinformatic pipeline
 * statistical analysis
 
 
Real data sets : 
 * download data from SRA
 * bioinformatic pipeline
 * statistical analysis
 
# Usage

To run the code,  you need to 
1. install [snakemake](https://snakemake.readthedocs.io/en/stable/getting_started/installation.html) and [conda](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html).
2. clone this repo : `git clone https://forgemia.inra.fr/benoit.goutorbe/shallow-shotgun-metagenomics.git`
3. download the [UHGG catalog](http://ftp.ebi.ac.uk/pub/databases/metagenomics/mgnify_genomes/human-gut/v1.0/), by executing `scripts/download_UHGG.sh` and index it for `bwa mem` aligner (please refer to dedicated [documentation](http://bio-bwa.sourceforge.net/bwa.shtml) to do so).
4. store your paired sequencing files (`*_1.fastq.gz` and `*_2.fastq.gz`) in `example/data/`
5. modify the paths in `example/params.yml` and `example/RunSnake.sh`, taking into account where you cloned the repo and stored UHGG.
6. run `example/RunSnake.sh`

**NB** : if you are interested only in code reproduction related to the paper, you may only have a look on standalone HTML files generated with Rmarkdown. To re-run them, one should build dedicated environment `conda env create -f conda_envs/custom_R.yml` and execute `SIMULATIONS/compiledoc.sh` or `REAL_DATA/compile_doc.sh`.
