#!/bin/bash
source ~/.bashrc
export PYTHONPATH=''

conda activate snakemake-5.10.0
mkdir -p ./.log/

PATH_TO_REPO="/path2repo/"

############################################################
##    FIRST STEP : execute the begining of pipeline       ##
##   that needs internet connection (no cluster mode)     ##
############################################################


snakemake \
--snakefile "$PATH_TO_REPO/scripts/simulations.snakefile" \
--configfile "$PATH_TO_REPO/SIMULATIONS/params.yml" \
--until "$PATH_TO_REPO/SIMULATIONS/data/DownloadComplete.txt" \
--keep-going \
--latency-wait 150 \
--verbose \
--printshellcmds \
--use-conda \
--conda-prefix "$PATH_TO_REPO/conda-envs/"


############################################################
##           SECOND STEP : execute the remaining          ##
## that doesn't need internet connection (cluster mode)   ##
############################################################

snakemake \
--snakefile "$PATH_TO_REPO/scripts/simulations.snakefile" \
--configfile "$PATH_TO_REPO/SIMULATIONS/params.yml" \
--keep-going \
--latency-wait 150 \
--verbose \
--printshellcmds \
--use-conda \
--conda-prefix "$PATH_TO_REPO/conda-envs/"
