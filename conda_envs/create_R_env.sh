#!/bin/bash


conda create -n custom_R \
-c conda-forge -c bioconda -c r \
r-base=4.0.3 \
r-rmarkdown \
r-tidyverse \
r-caret \
r-randomforest \
r-gridextra \
r-viridis \
r-scales \
r-ggextra \
r-ggpubr \
r-rmdformats \
r-kableextra \
r-xlsx \
bioconductor-phyloseq \
bioconductor-deseq2 \
bioconductor-curatedmetagenomicdata

conda activate custom_R
R -e 'if (!require(evabic,quietly=T,warn.conflicts=F)){install.packages("evabic",repos="https://cran.biotools.fr")}'
conda deactivate

conda env export -n custom_R --no-builds > custom_R.yml
