#!/bin/bash
source ~/.bashrc
export PYTHONPATH=''

conda activate snakemake-5.10.0
mkdir -p ./.log/

PATH_TO_REPO="/path2repo/"

snakemake \
--snakefile "$PATH_TO_REPO/scripts/real_data.snakefile" \
--configfile "$PATH_TO_REPO/REAL_DATA/Matson2018/params.yml" \
--keep-going \
--latency-wait 150 \
--verbose \
--printshellcmds \
--use-conda \
--conda-prefix "$PATH_TO_REPO/conda-envs/"

