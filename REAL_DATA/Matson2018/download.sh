#!/bin/bash
grep "WGS" SAMPLE_SHEET.tsv | cut -f6 | cut -d ";" -f1 > download_list.txt
grep "WGS" SAMPLE_SHEET.tsv | cut -f6 | cut -d ";" -f2 >> download_list.txt
sed -i -e 's/^/ftp:\/\//' download_list.txt
wget -P data/ -i download_list.txt
