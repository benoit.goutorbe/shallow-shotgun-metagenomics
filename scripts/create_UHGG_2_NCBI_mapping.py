#!/usr/bin/python
# -*- coding:utf-8 -*-


##################################
##           IMPORTS            ##
##################################

import numpy as np
from Bio import Entrez
from Bio import SeqIO
from Bio import Seq
import os
import urllib
import random as rd
from itertools import product
import sys
import re
import yaml
from argparse import ArgumentParser


####################################################
####           ARGUMENTS AND INPUTS             ####
####################################################


def config_parameters(): 
	use = "use : create_UHGG_2_NCBI_mapping -g PathToGenomesMetadata -u UHGG_to_NCBI_taxonomy_file" 
	parser = ArgumentParser(use) 
	parser.add_argument("-g", "--genomesMetadata", dest="PathToGenomesMetadata",required=True, help="path where to read file UHGG genomes metadata (genomes-all_metadata.tsv)")
	parser.add_argument("-u", "--mappingFile", dest="UHGG_to_NCBI_taxonomy_file",required=True, help="path where to write file UHGG to NCBI taxonomy mapping") 
	params = parser.parse_args() 
	return params 

params = config_parameters()

#INPUT_TABLE = "/work_projet/alphabio/shallow-shotgun-metagenomics/SIMULATIONS/ABUNDANCES.tsv"
#OUTPUT_TABLE = "/work_projet/alphabio/shallow-shotgun-metagenomics/SIMULATIONS/ABUNDANCES_translated.tsv"
#PathToGenomesMetadata = "/save_home/bgoutorbe/DATABASES/UHGG/genomes-all_metadata.tsv"
#MODE = "random"

####################################################
####           PARSE UHGG METADATA              ####
####################################################

genomes_metadata = [line.replace('\n','').split('\t') for line in open(params.PathToGenomesMetadata,"r").readlines()[1:]]

# retrieve lineage for each species
genomes_metadata_rep = [l for l in genomes_metadata if l[0]==l[16]]
UHGG_lineages = {l[0]:[t.split('__')[1] for t in l[18].split(';')] for l in genomes_metadata_rep}


# all genomes per species
genomes_per_species = {}
for g in genomes_metadata:
	try :
		genomes_per_species[g[16]].append(g[0])
	except KeyError:
		genomes_per_species[g[16]]=[g[0]]


#####################################################
##  ASSIGN TO EACH UHGG SPECIES A NCBI LINEAGE     ##
#####################################################

Entrez.email = 'benoit.goutorbe@inrae.fr'
taxLevels = ["superkingdom", "phylum", "class", "order", "family", "genus", "species"]

# function that retrieve lineage from NCBI taxID
def retrieve_lineage(tax_ID) :
	lineage = [''] * 7
	records = Entrez.read(Entrez.efetch(db = 'taxonomy', id=tax_ID))
	for taxon in records[0]['LineageEx']:
		if taxon['Rank'] in taxLevels:
			lineage[taxLevels.index(taxon['Rank'])] = taxon['ScientificName']
	if records[0]["Rank"] in taxLevels:
		lineage[taxLevels.index(records[0]["Rank"])] = records[0]['ScientificName']
	return lineage
	

# function that searches a taxa name in NCBI's taxonomy
def get_taxo(name):
	for attempt in range(10):
		try:
			name = name.replace('[', '').replace(']', '')
			records = Entrez.read(Entrez.esearch(db = 'taxonomy',term = name))
			if len(records["IdList"]) == 1:
				lineage = retrieve_lineage(records["IdList"][0])
				return lineage, 0
		except:
    			print('                     An error occurred.')
			continue
		break
	return [''] * 7, 1

# function that iteratively looks for NCBI's correspondance for a UHGG species
#	starting from it's species name, and then higher ranks
def get_nearest_NCBI_taxon(UHGG_ID) :
	lineage = UHGG_lineages[UHGG_ID]
	for i,tax in enumerate(reversed(lineage)) :
		if tax != '' :
			NCBI_lineage, code = get_taxo(tax)
			if code == 0 and NCBI_lineage[0] in ["Bacteria", "Archaea"] :
				print (UHGG_ID + '\t' +taxLevels[::-1][i] + '\t' + ';'.join(NCBI_lineage))
				return(NCBI_lineage)
			elif "_" in tax :
				tax_cleared = re.sub("_[ABCDEFGHIJKLMNOPQRSTUVWXYZ]","", tax)
				NCBI_lineage, code = get_taxo(tax_cleared)
				if code == 0 and NCBI_lineage[0] in ["Bacteria", "Archaea"] :
					print (UHGG_ID + '\t' +taxLevels[::-1][i] + '\t' + ';'.join(NCBI_lineage))
					return(NCBI_lineage)
	print("WARNING no match for " + UHGG_ID + " : " + ";".join(lineage))
	return([''] * 7)
	

UHGG_NCBI_mapping = {sp:get_nearest_NCBI_taxon(sp) for sp in UHGG_lineages.keys()}

#UHGG_to_NCBI_taxonomy_file="/save_home/bgoutorbe/DATABASES/UHGG/UHGG_to_NCBI_taxonomy.tsv"
content='\n'.join([k+'\t'+'|'.join(UHGG_NCBI_mapping[k]) for k in UHGG_NCBI_mapping])
open(params.UHGG_to_NCBI_taxonomy_file,"w").writelines(content)


