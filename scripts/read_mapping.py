#!/usr/bin/python
# -*- coding:utf-8 -*-

####################################################
####                 IMPORTS                    ####
####################################################

import sys
import pysam
import os
import numpy as np
from collections import Counter
import random as rd
import yaml
from argparse import ArgumentParser 

####################################################
####           ARGUMENTS AND INPUTS             ####
####################################################


def config_parameters(): 
	use = "use : ./read_mapping.py -i INPUT_BAM -o OUTPUT_BAM_UNIQ -t OUTPUT_MAPPING_TABLE -s OUTPUT_SUMMARY" 
	parser = ArgumentParser(use) 
	parser.add_argument("-i", "--inputBAM", dest="INPUT_BAM",default="", required=True, help="input BAM file ") 
	parser.add_argument("-o", "--outputBAM", dest="OUTPUT_BAM_UNIQ",default="", help="output BAM file containing reads that mapped consistenly only to one genome") 
	parser.add_argument("-t", "--outputTable", dest="OUTPUT_MAPPING_TABLE",required=True, default="", help="output file where to store results of mapping")
	parser.add_argument("-s", "--outputSummary", dest="OUTPUT_SUMMARY",default="", help="output file where to store summary of mapping")
	params = parser.parse_args() 
	#if len(sys.argv) != 5 :
	#	sys.exit("Warning : wrong number of arguements !!\n"+use)
	return params 

params = config_parameters()


####################################################
####           READ ALIGNMENT FILE              ####
####################################################


direction = lambda r : 'reverse' if r.is_read2 else 'forward'

mapping={}
for r in pysam.AlignmentFile(params.INPUT_BAM, "rb") :
	sens = direction(r)
	if r.reference_name != None :
		hits = [r.reference_name]
		# check XA tags (for "BWA"-style reporting of multimapping)
		if 'XA' in [tag[0]  for tag in r.tags] : 
			tags = {tag[0]: tag[1] for tag in r.tags} # parsing tags into a dictionnary 
			hits += [x.split(",")[0] for x in tags["XA"].split(";") if x != '']
		# retrieve genomeID from "reference_name" (its originally genomeID-contigID)
		hits = ['_'.join(h.split("_")[0:2]) for h in hits if h!='']
	else :
		hits = []
	try :
		mapping[r.qname][sens] += hits
		mapping[r.qname][sens+"_length"] = r.query_length
	except KeyError :
		mapping[r.qname] = {"forward":[], "reverse":[], "forward_length":0, "reverse_length":0}
		mapping[r.qname][sens] += hits
		mapping[r.qname][sens+"_length"] = r.query_length

# consistent_hits : for each read, we look for genomes hitten by R1 and R2
# 	and we store the results by hits
for k in mapping :
	mapping[k]['consistent_hits'] = [hit for hit in mapping[k]['forward'] if hit in mapping[k]['reverse']]
	mapping[k]['length'] = mapping[k]["forward_length"] + mapping[k]["reverse_length"]

uniquely_mapped_reads = {r:True for r in mapping if len(mapping[r]['consistent_hits'])==1}

def is_uniquely_mapped(r) :
	return(uniquely_mapped_reads.get(r, False))

# write results in a dedicated file
with open(params.OUTPUT_MAPPING_TABLE,'w') as file :
	file.write("\n".join([k+"\t"+str(mapping[k]['length'])+"\t" + ";".join(mapping[k]['consistent_hits']) for k in mapping])  + "\n")



####################################################
####              WRITE SUMMARY                 ####
####################################################

if params.OUTPUT_SUMMARY != "" :
	total = str(len(mapping))
	mapped = str(len([k for k in mapping if len(mapping[k]['consistent_hits'])!= 0]))
	mapped_unambiguously = str(len(uniquely_mapped_reads))
	#more_than_5_hits = str(len([k for k in mapping if len(mapping[k]['consistent_hits']) >= 5]))
	richness = str(len(set([mapping[k]["consistent_hits"][0] for k in mapping if len(mapping[k]["consistent_hits"])==1])))
	# write results in a dedicated file
	with open(params.OUTPUT_SUMMARY,'w') as file :
		output_content = "\t".join(["total", "mapped", "mapped_unambiguously", "richness"]) +"\n"
		output_content += "\t".join([total, mapped, mapped_unambiguously,richness])
		file.write(output_content  + "\n")


# release memory 
del(mapping)

####################################################
####  write BAM file with unambiguous reads     ####
####################################################

# write bam file with only consistently mapped and unambiguous reads

if params.OUTPUT_BAM_UNIQ != "" :
	bam = pysam.AlignmentFile(params.INPUT_BAM, "rb")
	bam_clear = pysam.AlignmentFile(params.OUTPUT_BAM_UNIQ, "wb", template=bam)
	for read in bam:
		if is_uniquely_mapped(read.qname):
			bam_clear.write(read)
	bam.close()
	bam_clear.close()


