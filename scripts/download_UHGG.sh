#!/bin/bash

wget http://ftp.ebi.ac.uk/pub/databases/metagenomics/mgnify_genomes/human-gut/v1.0/genomes-all_metadata.tsv
awk -F $"\t" '$1 == $17' <genomes-all_metadata.tsv > genomes-rep_metadata.tsv
cut -d$'\t' -f24 genomes-rep_metadata.tsv > download_list.txt
wget -P UHGG/ -i download_list.txt
for filename in UHGG/*.gff.gz; do zcat ${filename} | grep -v '#' | grep -v '\t' >> UHGG_reprentative_genomes.fna; done
