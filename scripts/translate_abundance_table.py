#!/usr/bin/python
# -*- coding:utf-8 -*-


##################################
##           IMPORTS            ##
##################################

import numpy as np
from Bio import Entrez
from Bio import SeqIO
from Bio import Seq
import os
import urllib
import random as rd
from itertools import product
import sys
import re
import yaml
from argparse import ArgumentParser


####################################################
####           ARGUMENTS AND INPUTS             ####
####################################################


def config_parameters(): 
	use = "use : translate_abundance_table.py -i INPUT_TABLE -o OUTPUT_TABLE -g PathToGenomesMetadata -u UHGG_to_NCBI_taxonomy_file -m mode" 
	parser = ArgumentParser(use) 
	parser.add_argument("-i", "--inputTable", dest="INPUT_TABLE", required=True, help="input abundances table file with species names in first column")
	parser.add_argument("-o", "--outputTable", dest="OUTPUT_TABLE", required=True, help="output abundances table file with UHGG genomes names in first column") 
	parser.add_argument("-g", "--genomesMetadata", dest="PathToGenomesMetadata",required=True, help="path to file UHGG genomes metadata (genomes-all_metadata.tsv)")
	parser.add_argument("-u", "--mappingFile", dest="UHGG_to_NCBI_taxonomy_file",required=True, help="path where to read file UHGG to NCBI taxonomy mapping") 
	parser.add_argument("-m", "--mode", dest="MODE",required=True, choices = ["representative", "random"], help="mode : 'representative' to use the representative genome of each species OR 'random' to use one randomly selected genome")
	params = parser.parse_args() 
	return params 

params = config_parameters()

#INPUT_TABLE = "/work_projet/alphabio/shallow-shotgun-metagenomics/SIMULATIONS/ABUNDANCES.tsv"
#OUTPUT_TABLE = "/work_projet/alphabio/shallow-shotgun-metagenomics/SIMULATIONS/ABUNDANCES_translated.tsv"
#PathToGenomesMetadata = "/save_home/bgoutorbe/DATABASES/UHGG/genomes-all_metadata.tsv"
#UHGG_to_NCBI_taxonomy_file="/save_home/bgoutorbe/DATABASES/UHGG/UHGG_to_NCBI_taxonomy.tsv"
#MODE = "random"

####################################################
####           PARSE UHGG METADATA              ####
####################################################

genomes_metadata = [line.replace('\n','').split('\t') for line in open(params.PathToGenomesMetadata,"r").readlines()[1:]]

# retrieve lineage for each species
genomes_metadata_rep = [l for l in genomes_metadata if l[0]==l[16]]
UHGG_lineages = {l[0]:[t.split('__')[1] for t in l[18].split(';')] for l in genomes_metadata_rep}


# all genomes per species
genomes_per_species = {}
for g in genomes_metadata:
	try :
		genomes_per_species[g[16]].append(g[0])
	except KeyError:
		genomes_per_species[g[16]]=[g[0]]


content = open(params.UHGG_to_NCBI_taxonomy_file,"r").readlines()
UHGG_NCBI_mapping = {line.split('\t')[0]:line.split('\t')[1].replace("\n","").split("|") for line in content}	


	


#####################################################
##  GET A NEIGHBOR FOR EACH INPUT NCBI SPECIES     ##
#####################################################
Entrez.email = 'benoit.goutorbe@inrae.fr'
taxLevels = ["superkingdom", "phylum", "class", "order", "family", "genus", "species"]

# function that retrieve lineage from NCBI taxID
def retrieve_lineage(tax_ID) :
	lineage = [''] * 7
	records = Entrez.read(Entrez.efetch(db = 'taxonomy', id=tax_ID))
	for taxon in records[0]['LineageEx']:
		if taxon['Rank'] in taxLevels:
			lineage[taxLevels.index(taxon['Rank'])] = taxon['ScientificName']
	if records[0]["Rank"] in taxLevels:
		lineage[taxLevels.index(records[0]["Rank"])] = records[0]['ScientificName']
	return lineage
	

# function that searches a taxa name in NCBI's taxonomy
def get_taxo(name):
	for attempt in range(10):
		try:
			name = name.replace('[', '').replace(']', '')
			records = Entrez.read(Entrez.esearch(db = 'taxonomy',term = name))
			if len(records["IdList"]) == 1:
				lineage = retrieve_lineage(records["IdList"][0])
				return lineage, 0
		except:
    			print('                     An error occurred.')
			continue
		break
	return [''] * 7, 1


# read the species list and search for their taxonomy
in_lines = open(params.INPUT_TABLE,"r").readlines()
species = [line.split('\t')[0] for line in in_lines[1:]]
sp2lineage = {sp:get_taxo(sp)[0] for sp in species}


# get the possible UHGG species (nearest from the input species list)
def get_correspodance_depth(lin1, lin2) :
	for i in range(len(lin1)):
		if lin1[i] != lin2[i] :
			return(i)
	return(7)


def get_nearest_UHGG_cluster(lineage) :
	sp=lineage[6]
	bests = []
	best_depth = 0
	for k in UHGG_NCBI_mapping :
		depth = get_correspodance_depth(lineage, UHGG_NCBI_mapping[k])
		if depth == best_depth :
			bests.append(k)		
		elif depth > best_depth :
			bests = [k]
			best_depth = depth
	#print(sp+"\t :\t"+'['+str(best_depth)+']:'+"/".join(bests))
	if best_depth != 7 :
		print("warning: \t"+ sp + " \t[NO UHGG species found, we used " + taxLevels[best_depth-1] + "]" )
	return({"uhgg_IDs":bests, "depth" : best_depth})

# sp_2_UHGG_cluster = {sp:rd.choice(get_nearest_UHGG_cluster(sp2lineage[sp])["uhgg_IDs"]) for sp in species}
sp_2_UHGG_cluster = {}
for sp in species :
	choices = get_nearest_UHGG_cluster(sp2lineage[sp])["uhgg_IDs"]
	choices = [c for c in choices if c not in sp_2_UHGG_cluster.values()]
	if len(choices) == 0 :
		print ("warning : all choices already present in selected species : "+ sp)
		choices = get_nearest_UHGG_cluster(sp2lineage[sp])["uhgg_IDs"]
	sp_2_UHGG_cluster[sp] = rd.choice(choices)


out_lines=[in_lines[0].replace('\n','')]
for line in in_lines[1:] :
	line_split = line.split('\t')
	rep = sp_2_UHGG_cluster[line_split[0]]
	if params.MODE == "representative" :
		genome_chosen = rep
	else :
		genome_chosen = rd.choice(genomes_per_species[rep])
	out_line = '\t'.join([genome_chosen]+ line_split[1:])
	out_lines.append(out_line.replace('\n',''))

open(params.OUTPUT_TABLE, "w").writelines("\n".join(out_lines))


