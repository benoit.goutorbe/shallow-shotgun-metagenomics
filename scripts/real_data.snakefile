shell.executable("/bin/bash")
shell.prefix("source ~/.bashrc; ")

# IMPORTS
import os
import sys
import random as rd
import gzip
import numpy 
import subprocess
WD=config["WorkingDir"]
ResultsDir=config["ResultsDir"]

if "SAMPLE_DATA" in config :
	SAMPLE_IDs = [l.split("\t")[0] for l in open(config["SAMPLE_DATA"],'r').readlines()[1:]]
else :
	all_fastq = [f for f in os.listdir(WD) if f.endswith(".fastq.gz")]
	all_ids = set([f.replace("_1.fastq.gz","").replace("_2.fastq.gz","") for f in all_fastq])
	SAMPLE_IDs = [id for id in all_ids  if id+"_1.fastq.gz" in all_fastq and id+"_2.fastq.gz" in all_fastq ]

print(SAMPLE_IDs)



##################################
##         PIPELINE             ##
##################################

ruleorder:  subsample_fastq > host_filter

GRAs_target = expand(ResultsDir+"{sampleID}.{mapper}.GRA.tsv", sampleID = SAMPLE_IDs, mapper=["bwa_mem"])
STATs_target = expand(ResultsDir+"{sampleID}.{mapper}.stats.tsv", sampleID = SAMPLE_IDs, mapper=["bwa_mem"])

if config["subsampling"]:
	GRAs_target += expand(ResultsDir+"{sampleID}_{depth}.{mapper}.GRA.tsv", sampleID = SAMPLE_IDs, mapper=["bwa_mem"], depth = config["depths_subsampling"])
	STATs_target += expand(ResultsDir+"{sampleID}_{depth}.{mapper}.stats.tsv", sampleID = SAMPLE_IDs, mapper=["bwa_mem"], depth = config["depths_subsampling"])


rule target:
	input:
		GRAs_target =  GRAs_target,
		STATs_target =  STATs_target,
		RDS = ResultsDir+config["DATASET_NAME"]+".RDS",
		HTML =ResultsDir+"report.html"
		
rule download_one:
	output:
		R1 = (WD+"{sampleID}_1.fastq.gz"),
		R2 = temp(WD+"{sampleID}_2.fastq.gz")
	params:
		download_list = WD+"../download_list.txt",
		WD = WD
	priority: 1
	shell:
		"grep {wildcards.sampleID} {params.download_list} | xargs wget -P {params.WD}"

rule trimmomatic:
	input:
		R1 = WD+"{sampleID}_1.fastq.gz",
		R2 = WD+"{sampleID}_1.fastq.gz"
	output:
		R1_paired = temp(WD+"{sampleID}_trimmed_filtered_1.fq"),
		R2_paired = temp(WD+"{sampleID}_trimmed_filtered_2.fq"),
		R1_unpaired = temp(WD+"{sampleID}_unpaired_trimmed_filtered_1.fq"),
		R2_unpaired = temp(WD+"{sampleID}_unpaired_trimmed_filtered_2.fq")
	threads: 8
	priority: 2
	params:
		Trimmomatic_options = config["Trimmomatic_options"]
	#conda:
	#	"conda_env/trimmomatic-0.39.yml"
	message:
		"trimmomatic ({wildcards.sampleID}): adapter trimming, quality trimming, N trimming, read length filtering"
	shell :
		"conda activate trimmomatic-0.39;"
		"trimmomatic PE "
		"-phred33 "
		"{input.R1} {input.R2} "
		"{output.R1_paired} {output.R1_unpaired} {output.R2_paired} {output.R2_unpaired} "
		"{params.Trimmomatic_options}"

rule host_filter:
	input:
		R1 = WD+"{sampleID}_trimmed_filtered_1.fq",
		R2 = WD+"{sampleID}_trimmed_filtered_2.fq"
	output:
		R1 = temp(WD+"{sampleID}_trimmed_filtered_hostfree_1.fq"),
		R2 = temp(WD+"{sampleID}_trimmed_filtered_hostfree_2.fq")
	threads: 8
	priority: 3
	#conda:
	#	"conda_env/NGS.yml"
	message:
		"BOWTIE2 ({wildcards.sampleID}): Filter out host DNA"
	shell :
		"conda activate NGS;"
		"bowtie2 "
		"-x /home/bgoutorbe/save/DATABASES/hg38/Bowtie2Index/genome "
		"--end-to-end "
		"--sensitive "
		"--threads {threads} "
		"-1 {input.R1} -2 {input.R2}|"
		"samtools view --threads {threads} -b -f12 -F256|"
		"samtools sort --threads {threads} -n|"
		"bedtools bamtofastq -i - -fq {output.R1} -fq2 {output.R2}"
		
		
rule subsample_fastq:
	input:
		R1 = WD+"{sampleID}_trimmed_filtered_hostfree_1.fq",
		R2 = WD+"{sampleID}_trimmed_filtered_hostfree_2.fq"
	output:
		R1 = temp(WD+"{sampleID}_{depth}_trimmed_filtered_hostfree_1.fq"),
		R2 = temp(WD+"{sampleID}_{depth}_trimmed_filtered_hostfree_2.fq")
	threads: 8
	priority: 3
	params :
		seed = 13
	message:
		"subsample_fastq_{wildcards.sampleID}_{wildcards.depth}"
	shell:
		"conda activate seqtk-1.3;"
		"NB=$(expr $(cat {input.R1} | wc -l) / 4) ;"
		"if [ $NB -gt {wildcards.depth} ] ;"
		"then " 
		"	seqtk sample -s {params.seed} {input.R1} {wildcards.depth} > {output.R1};"
		"	seqtk sample -s {params.seed} {input.R2} {wildcards.depth} > {output.R2};"
		"else " 
		"	echo '' > {output.R1};"
		"	echo '' > {output.R2};"
		"fi"

 
rule bwa_mem:
	input:
		R1 = WD+"{sampleID}_trimmed_filtered_hostfree_1.fq",
		R2 = WD+"{sampleID}_trimmed_filtered_hostfree_2.fq"
	output:
		SAM = temp(WD+"{sampleID}.bwa_mem.sam")
	threads: 8
	priority: 4
	params:
		BWA_index = config["BWA_index"]
	#conda:
	#	"conda_env/bwa-0.7.17.yml"
	message:
		"bwa_mem_{wildcards.sampleID}"
	shell:
		"conda activate bwa-0.7.17;"
		"bwa mem "
		"-h 50 " # if there are <INT hits with score >80% of the max score, output all in XA [5,200]
		"-t {threads} "
		"{params.BWA_index} "
		"{input.R1} {input.R2} "
		"> {output.SAM}"

rule sam2bam:
	input:
		SAM = WD+"{sampleID}.{mapper}.sam"
	output:
		BAM = temp(WD+"{sampleID}.{mapper}.bam")
	threads: 8
	priority: 5
	#conda:
	#	"conda_env/samtools-1.9.yml"
	message:
		"sam_to_bam_{wildcards.sampleID}_{wildcards.mapper}"
	shell:
		"conda activate samtools-1.9;"
		"samtools view --threads {threads} -b {input.SAM} > {output.BAM}"

rule read_mapping:
	input:
		BAM = WD+"{sampleID}.{mapper}.bam"
	output:
		BAM_UNIQ = temp(WD+"{sampleID}.{mapper}.uniq.bam"),
		MAPPING_TABLE = temp(WD+"{sampleID}.{mapper}.mapping.tsv"),
		SUMMARY = protected(ResultsDir+"{sampleID}.{mapper}.summary.tsv")
	threads: 4
	priority: 6
	params:
		ScriptsDir = config["ScriptsDir"]
	#conda:
	#	"conda_env/pysam-0.15.yml"
	message:
		"read_mapping_{wildcards.sampleID}_{wildcards.mapper}"
	shell:
		"conda activate pysam-0.15;"
		"python {params.ScriptsDir}/read_mapping.py -i {input.BAM} -o {output.BAM_UNIQ} -t {output.MAPPING_TABLE} -s {output.SUMMARY}"

rule sortBAM :
	input:
		WD+"{sampleID}.{mapper}.bam"
	output:
		temp(WD+"{sampleID}.{mapper}.sorted.ba")
	threads: 8
	priority: 7
	#conda:
	#	"conda_env/samtools-1.9.yml"
	message:
		"sort_bam_{wildcards.sampleID}_{wildcards.mapper}"
	shell:
		"conda activate samtools-1.9;"
		"samtools sort --threads {threads} {input} > {output}"


rule indexBAM :
	input:
		WD+"{sampleID}.{mapper}.sorted.ba"
	output:
		temp(WD+"{sampleID}.{mapper}.sorted.ba.bai")
	threads: 8
	priority: 8
	#conda:
	#	"conda_env/samtools-1.9.yml"
	message:
		"index_bam_{wildcards.sampleID}_{wildcards.mapper}"
	shell:
		"conda activate samtools-1.9;"
		"samtools index -@ {threads} {input}"

rule samtools_depth :
	input:
		BAM = WD+"{sampleID}.{mapper}.sorted.ba", 
		BAI = WD+"{sampleID}.{mapper}.sorted.ba.bai"
	output:
		temp(WD+"{sampleID}.{mapper}.depth.txt")
	threads: 8
	priority: 9
	#conda:
	#	"conda_env/samtools-1.9.yml"
	message:
		"samtools_detph_{wildcards.sampleID}_{wildcards.mapper}"
	shell:
		"conda activate samtools-1.9;"
		"samtools depth {input.BAM} | cut -d '_' -f1,2 | uniq -c > {output}"


rule build_GRA :
	input:
		DEPTH = WD+"{sampleID}.{mapper}.depth.txt",
		DEPTH_UNIQ = WD+"{sampleID}.{mapper}.uniq.depth.txt",
		MAPPING_TABLE = WD+"{sampleID}.{mapper}.mapping.tsv"
	output:
		STAT = protected(ResultsDir+"{sampleID}.{mapper}.stats.tsv"),
		GRA = protected(ResultsDir+"{sampleID}.{mapper}.GRA.tsv")
	params:
		UHGG_data = config["PathToGenomesMetadata"],
		ScriptsDir = config["ScriptsDir"]
	threads: 4
	priority: 10
	#conda:
	#	"conda_env/samtools-1.9.yml"
	message:
		"get_stat_per_genome_{wildcards.sampleID}_{wildcards.mapper}"
	shell:
			"python {params.ScriptsDir}/build_GRA.py -t {input.MAPPING_TABLE} -d {input.DEPTH} -e {input.DEPTH_UNIQ} -s {output.STAT} -g {output.GRA} -u {params.UHGG_data}"


rule apply_filters :
	input:
		GRAs_target =  GRAs_target,
		STATs_target =  STATs_target
	output:
		RDS = ResultsDir+config["DATASET_NAME"]+".RDS",
		HTML = ResultsDir+"report.html"
	#conda:
	#	"conda_env/custom_R.yml"
	params:
		RMD = config["ScriptsDir"] + "primary_analysis.Rmd",
		WD = WD,
		DATASET_NAME = config["DATASET_NAME"],
		SAMPLE_DATA = config["SAMPLE_DATA"] if "SAMPLE_DATA" in config else "",
		DEPTHS_SUBSAMPLING = config["depths_subsampling"] if config["subsampling"]==True else [],
		PATH_TO_BASIC_FILTERING_CUTOFF = config["PATH_TO_BASIC_FILTERING_CUTOFF"] if "PATH_TO_BASIC_FILTERING_CUTOFF" in config else "",
		UNIQ_COUNTS_CUTOFF = config["UNIQ_COUNTS_CUTOFF"] if "UNIQ_COUNTS_CUTOFF" in config else "",
		UNIQ_FRACTION_COVERED_CUTOFF = config["UNIQ_FRACTION_COVERED_CUTOFF"] if "UNIQ_FRACTION_COVERED_CUTOFF" in config else "",
		COUNTS_CUTOFF = config["COUNTS_CUTOFF"] if "COUNTS_CUTOFF" in config else "",
		FRACTION_COVERED_CUTOFF = config["FRACTION_COVERED_CUTOFF"] if "FRACTION_COVERED_CUTOFF" in config else "",
		ABUNDANCE_CUTOFF = config["ABUNDANCE_CUTOFF"] if "ABUNDANCE_CUTOFF" in config else "",
		PREVALENCE_CUTOFF = config["PREVALENCE_CUTOFF"] if "PREVALENCE_CUTOFF" in config else "",
		RF = config["RF_RDS"] if "RF_RDS" in config else "",
		RF_CUTOFF = config["RF_CUTOFF"] if "RF_CUTOFF" in config else "",
		PATH_TO_RF_CUTOFF = config["PATH_TO_RF_CUTOFF"] if "PATH_TO_RF_CUTOFF" in config else "",
		LOG_REG = config["LOG_REG_RDS"] if "LOG_REG_RDS" in config else "",
		LOG_REG_CUTOFF = config["LOG_REG_CUTOFF"] if "LOG_REG_CUTOFF" in config else "",
		PATH_TO_LOG_REG_CUTOFF = config["PATH_TO_LOG_REG_CUTOFF"] if "PATH_TO_LOG_REG_CUTOFF" in config else ""

	threads: 1
	message:
		"apply_filters"
	shell:
		"conda activate custom_R;"
		"R -e \""
		"rmarkdown::render('{params.RMD}',"
			"output_file='{output.HTML}',"
			"params = list(name = '{params.DATASET_NAME}',"
			"path_to_data = '{params.WD}',"
			"path_samples_metadata = '{params.SAMPLE_DATA}',"
			"depths_subsampling = '{params.DEPTHS_SUBSAMPLING}',"
			"path_to_basic_filtering_cutoff = '{params.PATH_TO_BASIC_FILTERING_CUTOFF}',"
			"uniq_counts_cutoff = '{params.UNIQ_COUNTS_CUTOFF}',"
			"uniq_fraction_covered_cutoff = '{params.UNIQ_FRACTION_COVERED_CUTOFF}',"
			"counts_cutoff = '{params.COUNTS_CUTOFF}',"
			"fraction_covered_cutoff = '{params.FRACTION_COVERED_CUTOFF}',"
			"abundance_cutoff = '{params.ABUNDANCE_CUTOFF}',"
			"prevalence_cutoff = '{params.PREVALENCE_CUTOFF}',"
			"path_RF = '{params.RF}',"
			"rf_cutoff = '{params.RF_CUTOFF}',"
			"path_to_rf_cutoff = '{params.PATH_TO_RF_CUTOFF}',"
			"path_log_reg = '{params.LOG_REG}',"
			"log_reg_cutoff = '{params.LOG_REG_CUTOFF}',"
			"path_to_log_reg_cutoff = '{params.PATH_TO_LOG_REG_CUTOFF}'))\";"
		"conda deactivate"

