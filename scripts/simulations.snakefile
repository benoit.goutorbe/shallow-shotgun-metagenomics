shell.executable("/bin/bash")
shell.prefix("source ~/.bashrc; ")

# IMPORTS
import os
import sys
import random as rd
import gzip
import numpy 
import subprocess

WD=config["WorkingDir"]

# parse genomes metadata
genomes_metadata = [line.replace('\n','').split('\t') for line in open(config["PathToGenomesMetadata"],"r").readlines()[1:]]
genomes_per_species = {}
for g in genomes_metadata:
	try :
		genomes_per_species[g[16]].append(g[0])
	except KeyError:
		genomes_per_species[g[16]]=[g[0]]

genomes_length = {g[0]:float(g[4]) for g in genomes_metadata}

def get_sampleIDs(path2file):
	try:
		return open(path2file,"r").readlines()[0].replace("\n","").split('\t')
	except IOError :
		return []
	
Sample_IDs = get_sampleIDs(WD+"ABUNDANCES_translated.tsv")
depths_subsampling = [d for d in config["depths"] if d!=max(config["depths"])]
depths = config["depths"]

##################################
##          PIPELINE            ##
##################################

ruleorder:  subsample_fastq > trimmomatic > split_R1_R2 > simulate_sequencing

				
rule target:
	input:
		abundance_table = WD+"ABUNDANCES_translated.tsv",
		GRAs = expand(WD+"GRA/{sampleID}_{depth}.{mapper}.GRA.tsv", sampleID = get_sampleIDs(WD+"ABUNDANCES_translated.tsv"), mapper=["bwa_mem"], depth = depths),
		HTML = WD+"results/analysis_simulations.html"

rule retrieve_profiles :
	output:
		WD+"ABUNDANCES.tsv"
	threads: 1
	#conda:
	#	"conda_env/custom_R.yml"
	params:
		script =  config["ScriptsDir"] +"retrieve_composition_table.R",
		CMD_dataset = 'QinN_2014.metaphlan_bugs_list.stool',
		N_SAMPLES_MAX = 100
	message:
		"Retrieving profiles (abundance tables) from curatedMetagenomicData"
	shell:
		"conda activate custom_R;"
		"Rscript {params.script} "
		"{params.CMD_dataset} "
		"{params.N_SAMPLES_MAX} "
		"{output}"


rule create_UHGG_NCBI_mapping :
	output:
		map=config["UHGG_to_NCBI_taxonomy_file"],
	threads: 1
	params:
		script =  config["ScriptsDir"] +"create_UHGG_2_NCBI_mapping.py",
		PathToGenomesMetadata = config["PathToGenomesMetadata"]
	message:
		"Creating UHGG 2 NCBI taxonomy mapping"
	shell:
		"python {params.script} "
		"-g {params.PathToGenomesMetadata} "
		"-u {output.map} "


rule translate_profiles :
	input:
		ab=WD+"ABUNDANCES.tsv",
		map=config["UHGG_to_NCBI_taxonomy_file"]
	output:
		ab=WD+"ABUNDANCES_translated.tsv"
	threads: 1
	params:
		script =  config["ScriptsDir"] +"translate_abundance_table.py",
		PathToGenomesMetadata = config["PathToGenomesMetadata"],
		mode = "random"
	message:
		"Translating profiles (abundance tables)"
	shell:
		"python {params.script} "
		"-i {input.ab} "
		"-o {output.ab} "				
		"-g {params.PathToGenomesMetadata} "	
		"-u {input.map} "
		"-m {params.mode}"


rule download_genomes:
	input:
		ab = WD+"ABUNDANCES_translated.tsv"
	output:
		check_download = WD+"DownloadComplete.txt"
	message:
		"Download genomes"
	run:
		shell("mkdir -p "+config["PathToGenomesStorage"])
		all_genomes = [line.split('\t')[0] for line in open(input.ab,'r').readlines()[1:]]
		#all_genomes = ["GUT_GENOME055631","GUT_GENOME173836","GUT_GENOME005580"]
		for g in all_genomes:
			if not os.path.exists(config["PathToGenomesStorage"]+g+".fasta") :
				url = genomes_metadata[[g_[0] for g_ in genomes_metadata].index(g)][23]
				#cmd="ssh -t goutorbe@10.36.1.135 'wget -P " + config["PathToGenomesStorage"] + " " + url +"'"
				cmd="wget -P " + config["PathToGenomesStorage"] + " " + url
				print(cmd)
				shell(cmd)
				gff = config["PathToGenomesStorage"] + g + ".gff.gz"
				fasta = config["PathToGenomesStorage"] + g + ".fasta"
				shell("zcat " + gff + " | grep -v '##' | grep -v '\\t' > " + fasta )
				shell("rm " + gff)
		if all([os.path.exists(config["PathToGenomesStorage"]+g+".fasta") for g in all_genomes]):
			open(output.check_download, 'w').write("all genomes were downlaoded : \n\t-"+ "\n\t-".join(all_genomes))
		

rule setup_simulation_per_sample:
	input : 
		UHGG_to_NCBI_taxonomy_file = config["UHGG_to_NCBI_taxonomy_file"],
		ab=WD+"ABUNDANCES_translated.tsv",
		check_download = WD+"DownloadComplete.txt"
	output:
		param_simul = WD+"params/params_{sampleID}.txt"
	message:
		"Set up all simulation for each sample({wildcards.sampleID})"
	threads: 1
	run :
		Sample_IDs = get_sampleIDs(input.ab)
		column = Sample_IDs.index(wildcards.sampleID) + 1
		output_lines=[]
		for line in open(input.ab,"r").readlines()[1:] :
			line_split=line.replace("\n","").split('\t')
			if float(line_split[column]) != 0 :
				output_lines.append(line_split[0]+'\t'+line_split[column]+'\n')
		open(output.param_simul, 'w').writelines(output_lines)


rule simulate_sequencing:
	input:
		param_simul = WD+"params/params_{sampleID}.txt",
		check_download = WD+"DownloadComplete.txt"
	output:
		FQ = temp(WD+"FASTQ/{sampleID}.raw.fq"),
		tempdir = temp(directory(WD+"FASTQ/{sampleID}/"))
	message:
		"Simulating sequencing ({wildcards.sampleID})"
	priority: 1
	params :
		grinder_params = "-length_bias 1 -rd 125 -insert_dist 500 normal 50 -fq 1 -ql 39 20",
		depth = 1.5*max(config['depths']), # we simulate more reads than needed, and then we will subsample to exact targeted depths
		path_genomes = config["PathToGenomesStorage"],
		path_fastq = WD+"FASTQ/tmp/"
	shell :
		"awk -v depth={params.depth} "
			"-v options=\"{params.grinder_params}\" "
			"-v path=\"{output.tempdir}/\" "
			"-v prefix=\"{wildcards.sampleID}-\" "
			"-v genomePath=\"{params.path_genomes}\" "
			"'{{ cmd=\"grinder -reference_file \" genomePath $1 \".fasta -total_reads \" 2*int(1+depth*$2/100) options \" -od \" path \" -bn \" prefix $1 ; system(cmd) }}' "
			"FS='\\t' {input.param_simul};"
		"awk -v path=\"{output.tempdir}/\" "
			"-v prefix=\"{wildcards.sampleID}-\"  "
			"'{{ cmd=\"cat \" path prefix $1 \"-reads.fastq\" ; system(cmd)}}' "
			"FS='\\t' {input.param_simul} > {output.FQ}"


rule split_R1_R2:
	input:
		FQ = WD+"FASTQ/{sampleID}.raw.fq"
	output:
		R1 = temp(WD+"FASTQ/{sampleID}_1.raw.fq"),
		R2 = temp(WD+"FASTQ/{sampleID}_2.raw.fq")
	message:
		"Split reads ({wildcards.sampleID})"
	priority: 2
	run :
		sens = 1
		with open(input.FQ,"r") as all_reads, open(output.R1, "w") as r1, open(output.R2, "w") as r2 :
			for line in all_reads :
				if line[0]=="@" :
					sens = int(line.split()[0].split("/")[1])
					line = "@"+line.split()[1].replace("reference=","") + "_" + line.split()[0].replace("@","")+ " " + line.split()[2] + "\n"
				if sens == 1 :
					r1.write(line)
				else :
					r2.write(line)


rule trimmomatic:
	input:
		R1 = WD+"FASTQ/{sampleID}_1.raw.fq",
		R2 = WD+"FASTQ/{sampleID}_2.raw.fq"
	output:
		R1_paired = WD+"FASTQ/{sampleID}_1.clean.fq",
		R2_paired = WD+"FASTQ/{sampleID}_2.clean.fq",
		R1_unpaired = temp(WD+"FASTQ/{sampleID}_unpaired_1.fq"),
		R2_unpaired = temp(WD+"FASTQ/{sampleID}_unpaired_2.fq")
	threads: 8
	priority: 3
	params:
		Trimmomatic_options = config["Trimmomatic_options"]
	#conda:
	#	"conda_env/trimmomatic-0.39.yml"
	message:
		"trimmomatic ({wildcards.sampleID}): adapter trimming, quality trimming, N trimming, read length filtering"
	shell :
		"conda activate trimmomatic-0.39;"
		"trimmomatic PE "
		"-phred33 "
		"{input.R1} {input.R2} "
		"{output.R1_paired} {output.R1_unpaired} {output.R2_paired} {output.R2_unpaired} "
		"{params.Trimmomatic_options}"



rule subsample_fastq:
	input:
		R1 = WD+"FASTQ/{sampleID}_1.clean.fq",
		R2 = WD+"FASTQ/{sampleID}_2.clean.fq"
	output:
		R1 = temp(WD+"FASTQ/{sampleID}_{depth}_1.fq"),
		R2 = temp(WD+"FASTQ/{sampleID}_{depth}_2.fq")
	threads: 8
	priority: 4
	params :
		seed = 13
	message:
		"subsample_fastq_{wildcards.sampleID}_{wildcards.depth}"
	shell:
		"conda activate seqtk-1.3;"
		"NB=$(expr $(cat {input.R1} | wc -l) / 4) ;"
		"if [ $NB -gt {wildcards.depth} ] ;"
		"	then seqtk sample -s {params.seed} {input.R1} {wildcards.depth} > {output.R1};"
		"	seqtk sample -s {params.seed} {input.R2} {wildcards.depth} > {output.R2};"
		"fi"

 
rule bwa_mem:
	input:
		R1 = WD+"FASTQ/{sampleID}_1.fq",
		R2 = WD+"FASTQ/{sampleID}_2.fq"
	output:
		SAM = temp(WD+"mapping/{sampleID}.bwa_mem.sam")
	threads: 8
	priority: 5
	params:
		BWA_index = config["BWA_index"]
	#conda:
	#	"conda_env/bwa-0.7.17.yml"
	message:
		"bwa_mem_{wildcards.sampleID}"
	shell:
		"conda activate bwa-0.7.17;"
		"bwa mem "
		"-h 50 " # if there are <INT hits with score >80% of the max score, output all in XA [5,200]
		"-t {threads} "
		"{params.BWA_index} "
		"{input.R1} {input.R2} "
		"> {output.SAM}"


rule sam2bam:
	input:
		SAM = WD+"mapping/{sampleID}.{mapper}.sam"
	output:
		BAM = temp(WD+"mapping/{sampleID}.{mapper}.bam")
	threads: 8
	priority: 6
	#conda:
	#	"conda_env/samtools-1.9.yml"
	message:
		"sam_to_bam_{wildcards.sampleID}_{wildcards.mapper}"
	shell:
		"conda activate samtools-1.9;"
		"samtools view --threads {threads} -b {input.SAM} > {output.BAM}"

rule read_mapping:
	input:
		BAM = WD+"mapping/{sampleID}.{mapper}.bam"
	output:
		BAM_UNIQ = temp(WD+"mapping/{sampleID}.{mapper}.uniq.bam"),
		MAPPING_TABLE = temp(WD+"mapping/{sampleID}.{mapper}.mapping.tsv"),
		SUMMARY = protected(WD+"summary/{sampleID}.{mapper}.summary.tsv")
	threads: 4
	priority: 7
	params:
		ScriptsDir = config["ScriptsDir"]
	#conda:
	#	"conda_env/pysam-0.15.yml"
	message:
		"read_mapping_{wildcards.sampleID}_{wildcards.mapper}"
	shell:
		"conda activate pysam-0.15;"
		"python {params.ScriptsDir}/read_mapping.py -i {input.BAM} -o {output.BAM_UNIQ} -t {output.MAPPING_TABLE} -s {output.SUMMARY}"

rule sortBAM :
	input:
		WD+"mapping/{sampleID}.{mapper}.bam"
	output:
		temp(WD+"mapping/{sampleID}.{mapper}.sorted.bam")
	threads: 8
	priority: 7
	#conda:
	#	"conda_env/samtools-1.9.yml"
	message:
		"sort_bam_{wildcards.sampleID}_{wildcards.mapper}"
	shell:
		"conda activate samtools-1.9;"
		"samtools sort --threads {threads} {input} > {output}"


rule indexBAM :
	input:
		WD+"mapping/{sampleID}.{mapper}.sorted.bam"
	output:
		temp(WD+"mapping/{sampleID}.{mapper}.sorted.bam.bai")
	threads: 8
	priority: 7
	#conda:
	#	"conda_env/samtools-1.9.yml"
	message:
		"index_bam_{wildcards.sampleID}_{wildcards.mapper}"
	shell:
		"conda activate samtools-1.9;"
		"samtools index -@ {threads} {input}"

rule samtools_depth :
	input:
		BAM = WD+"mapping/{sampleID}.{mapper}.sorted.bam", 
		BAI = WD+"mapping/{sampleID}.{mapper}.sorted.bam.bai"
	output:
		temp(WD+"mapping/{sampleID}.{mapper}.depth.txt")
	threads: 8
	priority: 8
	#conda:
	#	"conda_env/samtools-1.9.yml"
	message:
		"samtools_detph_{wildcards.sampleID}_{wildcards.mapper}"
	shell:
		"conda activate samtools-1.9;"
		"samtools depth {input.BAM} | cut -d '_' -f1,2 | uniq -c > {output}"


rule build_GRA :
	input:
		DEPTH = WD+"mapping/{sampleID}.{mapper}.depth.txt",
		DEPTH_UNIQ = WD+"mapping/{sampleID}.{mapper}.uniq.depth.txt",
		MAPPING_TABLE = WD+"mapping/{sampleID}.{mapper}.mapping.tsv"
	output:
		STAT = protected(WD+"cov/{sampleID}.{mapper}.stats.tsv"),
		GRA = protected(WD+"GRA/{sampleID}.{mapper}.GRA.tsv")
	params:
		UHGG_data = config["PathToGenomesMetadata"],
		ScriptsDir = config["ScriptsDir"]
	threads: 4
	priority: 9
	#conda:
	#	"conda_env/samtools-1.9.yml"
	message:
		"get_stat_per_genome_{wildcards.sampleID}_{wildcards.mapper}"
	shell:
			"python {params.ScriptsDir}/build_GRA.py -t {input.MAPPING_TABLE} -d {input.DEPTH} -e {input.DEPTH_UNIQ} -s {output.STAT} -g {output.GRA} -u {params.UHGG_data}"


rule analysis :
	input:
		params = expand(WD+"params/params_{sampleID}.txt", sampleID = get_sampleIDs(WD+"ABUNDANCES_translated.tsv")),
		GRAs = expand(WD+"GRA/{sampleID}_{depth}.{mapper}.GRA.tsv", sampleID = get_sampleIDs(WD+"ABUNDANCES_translated.tsv"), mapper=["bwa_mem"], depth = depths),
		cov = expand(WD+"cov/{sampleID}_{depth}.{mapper}.stats.tsv", sampleID = get_sampleIDs(WD+"ABUNDANCES_translated.tsv"), mapper=["bwa_mem"], depth = depths),
		summary = expand(WD+"summary/{sampleID}_{depth}.{mapper}.summary.tsv", sampleID = get_sampleIDs(WD+"ABUNDANCES_translated.tsv"), mapper=["bwa_mem"], depth = depths)
	output:
		html = protected(WD+"results/analysis_simulations.html")
	params:
		script = "analysis_simulations.Rmd",
		WD = WD, 
		UHGG_data = config["PathToGenomesMetadata"]
	threads: 4
	#conda:
	#	"conda_env/custom_R.yml"
	message:
		"Final analysis"
	shell:
			"conda activate custom_R;"
			"R -e \"rmarkdown::render('{params.script}',output_file='{output.html}',params = list(PATH_TO_DATA = '{params.WD}', PATH_TO_genomes_metadata = '{params.UHGG_data}'))\""


