#!/usr/bin/python
# -*- coding:utf-8 -*-

####################################################
####                 IMPORTS                    ####
####################################################

import sys
import pysam
import os
import numpy as np
from collections import Counter
import random as rd
import yaml
from argparse import ArgumentParser 

####################################################
####           ARGUMENTS AND INPUTS             ####
####################################################


def config_parameters(): 
	use = "use : ./read_mapping.py -t INPUT_MAPPING_TABLE -d INPUT_DEPTH -s OUTPUT_STAT -g OUTPUT_GRA -u UHGG_data -m 1" 
	parser = ArgumentParser(use) 
	parser.add_argument("-t", "--inputTABLE", dest="INPUT_MAPPING_TABLE",required=True,default="", help="input mapping table file ('read_mapping.py' output)") 
	parser.add_argument("-d", "--inputDEPTH", dest="INPUT_DEPTH",required=True,default="", help="input depth file ('samtools depth' output)") 
	parser.add_argument("-e", "--inputDEPTH_uniq", dest="INPUT_DEPTH_UNIQ",required=True,default="", help="input depth file correspond to uniquely mapped reads ('samtools depth' output)") 
	parser.add_argument("-s", "--outputSTAT", dest="OUTPUT_STAT",required=True, default="", help="output file where to store statistics for each target genome")
	parser.add_argument("-g", "--outputGRA", dest="OUTPUT_GRA",required=True,default="", help="output file where to store genomes relative abundances (GRA)")
	parser.add_argument("-u", "--genomesMetadata", dest="UHGG_data",required=True,default="", help="file containing UHGG's reference genomes' metadata")
	parser.add_argument("-m", "--reallocationMode", dest="REALLOCATION_MODE",required=False, type=int,default=2,choices=[0, 1, 2], help="reallocation of amibuguous reads process : 0 (none) OR 1 (split [DEFAULT]) OR 2 (choose one)")
	params = parser.parse_args() 
	#if len(sys.argv) != 5 :
	#	sys.exit("Warning : wrong number of arguements !!\n"+use)
	return params 

params = config_parameters()


####################################################
####            READ UHGG METADATA              ####
####################################################

# get reference genomes' informations
with open(params.UHGG_data,"r") as file :
	content = file.readlines()[1:]
	genome_names = [line.split('\t')[0] for line in content]
	genome_names_rep = [line.split('\t')[0] for line in content if line.split('\t')[0] == line.split('\t')[16]]
	genome_lengths = {line.split('\t')[0] : int(line.split('\t')[4]) for line in content}

stats = {g:{"counts" : 0, "uniq_counts" : 0,"fraction_covered":0,  "uniq_fraction_covered":0, "coverage":0, "uniq_coverage" : 0, "GRA":0, "uniq_GRA":0} for g in genome_names_rep}

####################################################
####                EARLY STOP                  ####
####################################################
if open(params.INPUT_MAPPING_TABLE, "r").readlines() == ["\n"] :
	with open(params.OUTPUT_STAT,'w') as file :
		output_content = "\t".join(["genomeID", "counts","uniq_counts", "fraction_covered", "uniq_fraction_covered"]) +"\n"
		file.write(output_content)
	with open(params.OUTPUT_GRA,'w') as file :
		output_content = ""
		output_content += '\t'.join(["sample"]+genome_names_rep)
		complete_name = os.path.basename(params.OUTPUT_GRA).replace(".GRA.tsv", "")
		output_content += '\n' + complete_name +".uniq_GRA\t" + "\t".join(["0" for k in genome_names_rep])
		output_content += '\n' + complete_name +".GRA\t" + "\t".join(["0" for k in genome_names_rep])
		file.writelines(output_content + "\n")
	os._exit(0)

####################################################
####            READ INPUT FILES                ####
####################################################

depth_content = [line.lstrip().replace("\n","").split(" ") for line in open(params.INPUT_DEPTH, "r").readlines()]
for l in depth_content :
	stats[l[1]]["fraction_covered"] = float(l[0])/genome_lengths[l[1]]
del(depth_content)

uniq_depth_content = [line.lstrip().replace("\n","").split(" ") for line in open(params.INPUT_DEPTH_UNIQ, "r").readlines()]
for l in uniq_depth_content :
	stats[l[1]]["uniq_fraction_covered"] = float(l[0])/genome_lengths[l[1]]
del(uniq_depth_content)

table_content = [line.replace("\n","").split("\t") for line in open(params.INPUT_MAPPING_TABLE, "r").readlines()]
for l in table_content : 
	if len(l[2]) == 0 : continue
	hits = l[2].split(";")
	if len(hits)==1 :
		stats[hits[0]]["counts"] += 1
		stats[hits[0]]["uniq_counts"] += 1
		stats[hits[0]]["uniq_coverage"] += int(l[1])
	else :
		for k in hits:
			stats[k]["counts"] += 1

# coverage(i) = sum(reads mapped to g_i)/length(g_i)
for k in stats:
	stats[k]["uniq_coverage"] = float(stats[k]["uniq_coverage"])/genome_lengths[k]


# GRA_i = coverage(i)/sum(coverage)
norm_factor = sum([stats[k]["uniq_coverage"] for k in stats])
for k in stats:
	stats[k]["uniq_GRA"] = stats[k]["uniq_coverage"]/norm_factor

####################################################
####      REALLOCATION of ambiguous reads       ####
####################################################

if params.REALLOCATION_MODE == 0 :
	for k in stats:
		stats[k]["GRA"] = stats[k]["uniq_GRA"]

if params.REALLOCATION_MODE == 1 :
	for l in table_content :
		hits = l[2].split(";")
		length = int(l[1])
		if len(l[2]) == 0 :
			continue
		if len(hits)==1 :
			stats[hits[0]]["coverage"] += length
			continue
		probas = [stats[k]["uniq_GRA"] for k in hits]
		if sum(probas) != 0 :
			probas = [p/sum(probas) for p in probas]
			for i in range(len(hits)):
				stats[hits[i]]["coverage"] += length*probas[i]
	# coverage(i) = sum(reads mapped to g_i)/length(g_i)
	for k in stats:
		stats[k]["coverage"] = float(stats[k]["coverage"])/genome_lengths[k]
	# GRA_i = coverage(i)/sum(coverage)
	norm_factor = sum([stats[k]["coverage"] for k in stats])
	for k in stats:
		stats[k]["GRA"] = stats[k]["coverage"]/norm_factor

if params.REALLOCATION_MODE == 2 :
	for l in table_content :
		hits = l[2].split(";")
		length = int(l[1])
		if len(l[2]) == 0 :
			continue
		if len(hits)==1 :
			stats[hits[0]]["coverage"] += length
			continue
		probas = [stats[k]["uniq_GRA"] for k in hits]
		if sum(probas) != 0 :
			probas = [p/sum(probas) for p in probas]
			g = np.random.choice(hits, p = probas)
			stats[g]["coverage"] += length
	# coverage(i) = sum(reads mapped to g_i)/length(g_i)
	for k in stats:
		stats[k]["coverage"] = float(stats[k]["coverage"])/genome_lengths[k]
	# GRA_i = coverage(i)/sum(coverage)
	norm_factor = sum([stats[k]["coverage"] for k in stats])
	for k in stats:
		stats[k]["GRA"] = stats[k]["coverage"]/norm_factor


####################################################
####            WRITE output files              ####
####################################################

with open(params.OUTPUT_STAT,'w') as file :
	output_content = "\t".join(["genomeID", "counts","uniq_counts", "fraction_covered", "uniq_fraction_covered"]) +"\n"
	output_content += "\n".join(["\t".join([k,str(stats[k]["counts"]),str(stats[k]["uniq_counts"]),str(stats[k]["fraction_covered"]),str(stats[k]["uniq_fraction_covered"])]) for k in stats if stats[k]["uniq_counts"]!=0 ]) + "\n"
	file.write(output_content)


with open(params.OUTPUT_GRA,'w') as file :
	output_content = ""
	output_content += '\t'.join(["sample"]+genome_names_rep)
	complete_name = os.path.basename(params.OUTPUT_GRA).replace(".GRA.tsv", "")
	output_content += '\n' + complete_name +".uniq_GRA\t" + "\t".join([str(stats[k]["uniq_GRA"]) for k in genome_names_rep])
	output_content += '\n' + complete_name +".GRA\t" + "\t".join([str(stats[k]["GRA"]) for k in genome_names_rep])
	file.writelines(output_content + "\n")

